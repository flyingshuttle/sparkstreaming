package com.balab


import java.util.regex.Matcher

import com.balab.util.Utilities._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}
/**
  * Created by dev-env on 26/5/16.
  */
class LogParser {


  def logParser() = {


    setupTwitter()

    val ssc  = new StreamingContext("local[*]", "Logparser", Seconds(1))

    setupLogging()

    val pattern = apacheLogPattern()

    val lines = ssc.socketTextStream("10.211.55.13", 9999, StorageLevel.MEMORY_AND_DISK_SER)

    val requests  = lines.map( x => {

      val matcher:Matcher = pattern.matcher(x)

      if(matcher.matches()) {
        matcher.group(5)
      }
    })


    val urls = requests.map( x => {
      val words = x.toString.split(" ");

      if(words.length == 3) words(1) else "Error"

    })



    val urlCounts = urls.map( x => (x, 1)).reduceByKeyAndWindow( (x, y ) => x + y, (x, y) => x - y, Seconds(300), Seconds(1) )

    val sortedResults = urlCounts.transform(rdd => rdd.sortBy(x => x._2, false))

    sortedResults.print()


    ssc.checkpoint("/Users/dev-env/dev/learning/SparkStreamingExp/tst")
    ssc.start()
    ssc.awaitTermination()



  }

}


object LogParser {

  def main(arg:Array[String]) = {
    val logParser = new LogParser()
    logParser.logParser()
  }

}