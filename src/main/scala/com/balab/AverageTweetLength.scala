package com.balab



import java.util.concurrent.atomic.AtomicLong

import com.balab.util.Utilities._
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}


/**
  * Created by dev-env on 25/5/16.
  */
class AverageTweetLength {

  def findLength() = {

    setupTwitter()

    val ssc = new StreamingContext("local[*]", "AverageTweetLength", Seconds(1))

    setupLogging()

    val tweets = TwitterUtils.createStream(ssc, None)

    val statuses = tweets.map( status => status.getText())

    val lengths = statuses.map( status => status.length)


    val totalTweets = new AtomicLong()
    val totalChars = new AtomicLong()


    lengths.foreachRDD((rdd, time) => {

      val count = rdd.count()

      if(count > 0) {

        totalTweets.getAndAdd(count)

        totalChars.getAndAdd( rdd.reduce( (x , y) => x + y  ))

        println("Total tweets : " + totalTweets.get() +
                "Total Char   : " + totalChars.get() +
                "Average      : " + totalChars.get() / totalTweets.get() )

      }
    })

    ssc.start()
    ssc.awaitTermination()
  }
}



object AverageTweetLength {


  def main(arg:Array[String]) = {

      val averageTweetLength = new AverageTweetLength()

      averageTweetLength.findLength
  }
}