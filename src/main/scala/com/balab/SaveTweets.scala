package com.balab


import com.balab.util.Utilities._
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by dev-env on 24/5/16.
  */
class SaveTweets {

  def save() = {

    // Configure Twitter credentials using twitter.txt
    setupTwitter()

    val  ssc = new StreamingContext("local[*]", "SaveTweets", Seconds(1))

    setupLogging()

    val tweets  = TwitterUtils.createStream(ssc, None)


   val statuses = tweets.map(status => status.getText) // DAG

    var totalTweet:Long = 0

    statuses.print()


    statuses.foreachRDD((rdd, time) => {

      if(rdd.count() > 0 ) {


        val repartiionedRDD = rdd.repartition(1).cache()
        repartiionedRDD.saveAsTextFile("Twweets_"+time.milliseconds.toString)

        totalTweet += repartiionedRDD.count()

        println("Tweet Count : " + totalTweet)

        if(totalTweet > 1000 ) {
          System.exit(0)
        }

      }

    })

    ssc.start()
    ssc.awaitTermination()

  }


}

object SaveTweets {


  def main(arg:Array[String]) = {

   val saveTweets =  new SaveTweets()

    saveTweets.save()

  }
}