package com.balab

import java.util.regex.Matcher

import com.balab.util.Utilities._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.util.Try

/**
  * Created by dev-env on 26/5/16.
  */
class LogAlarmer {



  def alarmer() = {


    setupTwitter()

    val ssc  = new StreamingContext("local[*]", "Logparser", Seconds(1))

    setupLogging()

    val pattern = apacheLogPattern()

    val lines = ssc.socketTextStream("10.211.55.13", 9999, StorageLevel.MEMORY_AND_DISK_SER)

    val statuses  = lines.map( x => {

      val matcher:Matcher = pattern.matcher(x)

      if(matcher.matches()) matcher.group(6) else "[Error]"

    })

    val successFailure = statuses.map( x => {

      val statusCode = Try(x.toInt).getOrElse( 0 )

      if(statusCode >= 200 && statusCode < 300 ) {
        "Success"
      }else if(statusCode >= 500 && statusCode < 600 )  {
        "Failure"
      } else {
        "Other"
      }

    })



    val statusCount = successFailure.countByValueAndWindow(Seconds(300), Seconds(1))


    var totalSuccess:Long = 0
    var totalFailure:Long = 0

    statusCount.foreachRDD( (rdd, time) => {


       if(rdd.count() > 0 ) {

         val elements = rdd.collect()

         for(element <- elements) {
           val result = element._1
           val count = element._2

           result match {
             case "Success" => totalSuccess  += count
             case "Failure" =>  totalFailure += count
             case _  =>  println(result)
           }

         }

       }

    } )

    println("Total Success " + totalSuccess + "Total Failure " + totalFailure )

    if(totalSuccess + totalFailure > 100) {

      val ratio:Double = Try(totalSuccess.toDouble / totalFailure.toDouble).getOrElse(0)

      if(ratio > 0.5 ) {

        println("Something going wrong Wakeup you nasty..  ")

      } else {

        println("All System good " )

      }
    }

    ssc.checkpoint("/Users/dev-env/dev/learning/SparkStreamingExp/tst")
    ssc.start()
    ssc.awaitTermination()


  }

}


object LogAlarmer {


  def main(arg:Array[String]) = {

    val logAlarmer = new LogAlarmer()
    logAlarmer.alarmer()

  }
 }