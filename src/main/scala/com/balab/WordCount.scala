package com.balab

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by dev-env on 21/5/16.
  */


object WordCount {

  def main(arg: Array[String]) = {

    val conf = new SparkConf().setAppName("WordCount")
    conf.setMaster("local[*]")

    val sc = new SparkContext(conf)

    val input  = sc.textFile("book.txt")

    val words = input.flatMap(line => line.split(' '))

    val lowerCaseWords =  words.map( e => e.toLowerCase)

    val wordCount = lowerCaseWords.countByValue()

    val sample = wordCount.take(20)

    for((word, count) <- sample) {

      println(word + " " + count)

    }

    sc.stop()

  }

}
