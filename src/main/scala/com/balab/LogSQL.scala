package com.balab

import java.util.regex.Matcher

import com.balab.util.Utilities._
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.util.Try

/**
  * Created by dev-env on 27/5/16.
  */
class LogSQL {



  def log() = {

    setupTwitter()

    val ssc  = new StreamingContext("local[*]", "Logparser", Seconds(1))

    setupLogging()

    val pattern = apacheLogPattern()

    val lines = ssc.socketTextStream("10.211.55.13", 9999, StorageLevel.MEMORY_AND_DISK_SER)

    val requests  = lines.map( x => {

      val matcher:Matcher = pattern.matcher(x)

      if(matcher.matches()) {
        val request = matcher.group(5)
        val requestFiled = request.toString.split(" ")
        val url = Try(requestFiled(1)) getOrElse( "[error]")
        (url, matcher.group(6).toInt, matcher.group(9))
      } else {

        ("error", 0, "error")
      }
    })

    requests.foreachRDD( ( rdd, time ) => {


      val sqlContext = SQLContextSingleton.getInstance(rdd.sparkContext)
      import sqlContext.implicits._

      val requestDataFrame = rdd.map(w => Record(w._1, w._2, w._3)).toDF()
      requestDataFrame.registerTempTable("requests")

      val wordCountsDataFrame = sqlContext.sql("select agent, count(*) as total from requests group by agent")
      println(s"========= $time =========")
      wordCountsDataFrame.show()

    })

    ssc.checkpoint("/Users/dev-env/dev/learning/SparkStreamingExp/tst")
    ssc.start()
    ssc.awaitTermination()

  }
}

case class Record(url: String, status: Int, agent: String)

object SQLContextSingleton {

  @transient  private var instance: SQLContext = _

  def getInstance(sparkContext: SparkContext): SQLContext = {
    if (instance == null) {
      instance = new SQLContext(sparkContext)
    }
    instance
  }
}

object  LogSQL {


  def main(arg:Array[String]) = {

    val logsql = new LogSQL()
    logsql.log()
  }
}
