package com.balab

import java.util.regex.Matcher

import com.balab.util.Utilities._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming._

import scala.util.Try
/**
  * Created by dev-env on 31/5/16.
  */
object Sessionizer {

  case class SessionData(val sessionLength:Long, val clickstream:List[String] )

  def trackStatefunc(batchTime: Time, ip:String, url:Option[String], state: State[SessionData]):Option[( String, SessionData  )] = {

    val previousState = state.getOption().getOrElse( SessionData(0, List()))

    val newState = SessionData(previousState.sessionLength + 1, (previousState.clickstream :+ url.getOrElse("Empty")).take(10))

    state.update(newState)

    Some(ip, newState)

  }

  def main(args:Array[String]) =  {

    val ssc  = new StreamingContext("local[*]", "Sessionizer", Seconds(1))

    setupLogging()

    val pattern = apacheLogPattern()

    val stateSpec = StateSpec.function(trackStatefunc _).timeout(Minutes(3))

    val lines = ssc.socketTextStream("10.211.55.13", 9999, StorageLevel.MEMORY_AND_DISK_SER)

    val requests  = lines.map( x => {

      val matcher:Matcher = pattern.matcher(x)

      if(matcher.matches()) {
        val ip = matcher.group(1)
        val request = matcher.group(5)
        val requestFiled = request.toString.split(" ")
        val url = Try(requestFiled(1)) getOrElse( "[error]")
        (ip, url)
      } else {
        ("error", "error")
      }
    })

    val requestWithStatea =  requests.mapWithState(stateSpec)

    val  stateSnapShotStream  = requestWithStatea.stateSnapshots()

    val requestWithState = stateSnapShotStream.foreachRDD( ( rdd, time) => {


      val sqlContext = SQLContextSingleton.getInstance(rdd.sparkContext)
      import sqlContext.implicits._

      val requestsDataFrame = rdd.map(x => (x._1, x._2.sessionLength, x._2.clickstream) ).toDF("ip", "sessionLength", "clickstream")
      requestsDataFrame.registerTempTable("sessionData")

      val sessionDataFrame = sqlContext.sql("select * from sessionData")

      sessionDataFrame.show()

    } )


    ssc.checkpoint("/Users/dev-env/dev/learning/SparkStreamingExp/tst")
    ssc.start()
    ssc.awaitTermination()

  }

}
