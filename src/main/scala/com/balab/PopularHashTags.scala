package com.balab


import com.balab.util.Utilities._
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by dev-env on 25/5/16.
  */
class PopularHashTags {


  def find() = {

    setupTwitter()

    val  ssc  = new StreamingContext("local[*]", "PopularHashTags", Seconds(1))

    setupLogging()



    val tweets = TwitterUtils.createStream(ssc, None)

    val  statuses = tweets.map( status => status.getText)

    val   statusFlatMap = statuses.flatMap(text => text.split(" ") )

    val   statusHashTag = statusFlatMap.filter(word => word.startsWith("#")  )

    val   hashTagKeyValues = statusHashTag.map(tag => (tag, 1))

    val reducedTags  = hashTagKeyValues.reduceByKeyAndWindow((x,y) => x + y, (x,y) => x - y, Seconds(3000), Seconds(1)  )

    val soretedResults = reducedTags.transform(rdd => rdd.sortBy( x => x._2, false ))
    //val tweetTag = tweets.filter( words => words.getText().startsWith("#"))
    soretedResults.print()

    ssc.checkpoint("/Users/dev-env/dev/learning/SparkStreamingExp")

    ssc.start()
    ssc.awaitTermination()
    ssc.stop()


  }

}

object PopularHashTags {


  def main(arg:Array[String]) = {

     val popularHashTags = new PopularHashTags()

     popularHashTags.find()
  }
}
