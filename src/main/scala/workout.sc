val theUltimateAnswer:String  = "To life, the  universe, anf everything is 42."

val pattern = """.* ([\d]+).*""".r

val pattern(answerString) = theUltimateAnswer

val answer = answerString.toInt


//Key value pair
val picardShip = "picard" -> "ship"
picardShip._2

val shipList = List("picard","ship", "bala", "babu", "gomathi", "suthaaman")

//shipList(1)

//shipList.head

//.tail


for(ship <- shipList ) {println(ship)}


val backwardShips = shipList.map( (ship:String) => {ship+"s"})

val numberList = List(1, 2, 3, 4, 5)

val sum = numberList.reduce(( x: Int, y: Int) => {

      println(x +"  "+y )
      x + y
})


val iHatefive = numberList.filter((x:Int) => {x!= 5})

val iHatefiveq = numberList.filter( _ != 5 )