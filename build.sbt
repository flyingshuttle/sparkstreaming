name := "spark-streaming-exp"

organization := "FlyingShuttle"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.10.5"


libraryDependencies ++= {
  val sparkV = "1.6.1"
  Seq(
    "org.apache.spark" % "spark-core_2.10" % sparkV,
    "org.apache.spark" % "spark-sql_2.10" % sparkV,
    "org.apache.spark" % "spark-streaming_2.10" % sparkV,
    "org.apache.spark" % "spark-streaming-twitter_2.10" % sparkV,
    "org.scalatest" %% "scalatest" % "2.2.1" % "test",
    "org.scalacheck" %% "scalacheck" % "1.11.5" % "test"
  )
}

mainClass in (Compile, run) := Some("com.balab.PrintTweets")
